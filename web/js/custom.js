$(document).ready(function(){
	
	// if local storage is present, get the step
	if(typeof Storage !== "undefined") {
    	if (localStorage.getItem("step") !== null) {
    		step = localStorage.getItem("step");
    	}
		populateForm();	
 	}

    // Submit button
    var btnFinish = $('<button type="button"></button>').text('Register')
         .addClass('btn btn-info btn-finish disabled')
         .on('click', function(){
                if( !$(this).hasClass('disabled')){
                    var elmForm = $("#form-step-2");
                    if(elmForm){
                        elmForm.validator('validate');
                        var elmErr = elmForm.find('.has-error');
                        if(elmErr && elmErr.length > 0){
                            alert('Oops we still have error in the form');
                            return false;
                        }else{
                            //alert('Great! we are ready to submit form');
                            submitForm();
                        }
                    }
                }
            });
    

    // Form wizard
    $('#smartwizard').smartWizard({
            selected: step,
            theme: 'dots',
            transitionEffect:'fade',
            toolbarSettings: {toolbarPosition: 'bottom',
                              toolbarExtraButtons: [btnFinish]
                            },
            anchorSettings: {
                        markDoneStep: true, // add done css
                        markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
                        removeDoneStepOnNavigateBack: true, // While navigate back done step after active step will be cleared
                        enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
                    }
         });


    // validate going to next step & store the entered data
    $("#smartwizard").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
        var elmForm = $("#form-step-" + stepNumber);
        // only on forward navigation
        if(stepDirection === 'forward' && elmForm){
            elmForm.validator('validate');
            var elmErr = elmForm.children('.has-error');
            if(elmErr && elmErr.length > 0){
                // Form validation failed
                return false;
            }
            if(typeof Storage !== "undefined")
              {
              		storeFormData();
              		localStorage.setItem("step", stepNumber);     
              }
            else
              {
              	storeInSession(stepNumber);
              }
        }
        return true;
    });


    // show finish button on last page only
    $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection) {
        // Enable finish button only on last step
        if(stepNumber == 2){
            $('.btn-finish').removeClass('disabled');
        }else{
            $('.btn-finish').addClass('disabled');
        }
    });

    if (window.location.hash.substr(1) === 'step-3') {
    	$('.btn-finish').removeClass('disabled');
    }

    // store form data to local storage
    function storeFormData() {
        let formData  = $("#regForm").find(":input").filter(function () {
 			// don't store csrf
            return ($.trim(this.value).length > 0 && this.name !== "_csrf");
        }).serializeArray();
  		localStorage.setItem("formData", JSON.stringify(formData));
    }

    // populate form with data from local storage
    function populateForm() {
    	if (localStorage.getItem("formData") !== null) {
    	    let data = JSON.parse(localStorage.formData);
    		$.each(data, function( index, item ) {
    		  $(item.name).val('ss');
    		  $('[name="'+item.name+'"]').val(item.value);
    		});
    	}
   		
    }

    
    // send form data to store in server
    function storeInSession(step) {
    	var formData = $("input[name!=_csrf]", "#regForm").serialize();
    	formData += "&step="+step;
    	$.ajax({
    	    url: rootUrl+'/index.php?r=user/store-in-session',
    	    type: 'GET',
    	    data: formData,
    	    success: function (data) {
    	         return true;
    	    }
    	});
    }



    // submit the form
    function submitForm() {
    	var formData = $("#regForm").serialize();
    	$.ajax({
    	    url: rootUrl+'/index.php?r=user/create',
    	    type: 'POST',
    	    data: formData,
    	    success: function (data) {
    	        apiCompleted(data);
    	    },
    	    error: function () {
    	        alert("Something went wrong");
    	    }
    	});
    	// return false;
    }


    // Api call completed. Replace form content according to the response
    function apiCompleted(data) {
    	let content = ""
	    if (data.status === "success") {
	    	clearLocalData();
	    	content += "<h1 class='success'>Successfully Registered!</h1>";
	    	content += "<p class='success'>Payment ID:"+data.data+"</p>";
	    }
	    else if(data.status === "failure") {
	    	content += "<h1 class='success'>Sorry. We couldn't process the payment</h1>";
	    	$.each(data.data, function( index, item ) {
			  content += "<p class='success'>"+index.replace(/_/g," ")+": "+item+"</p>";
			});	
	    }
	    else {
	    	content += "<h1 class='success'>Something went wrong. Please try again.</h1>";
	    }
	    $(".user-create").html(content);
    }

    // clear locally stored formData and step
    function clearLocalData() {
    	if(typeof Storage !== "undefined") {
    		localStorage.removeItem('step');
    		localStorage.removeItem('formData');
    	}
    }

});

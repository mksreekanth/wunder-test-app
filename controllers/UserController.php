<?php

namespace app\controllers;

use Yii;
use app\Models\User;
use app\Models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use GuzzleHttp\Client;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    protected function apiUrl()
    {
        return 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';
    }


    protected function payFee($url, $customerId, $owner, $iban)
    {
        $client = new Client();

        $response = $client->request('POST', $url, [
            'json' => ['customerId' => $customerId, 'iban' => $iban, 'owner' => $owner]
        ]);

        return $response;
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {    
        $model = new User();
        $step = 0;

        $session = Yii::$app->session;
        if ($session->has('formData')){
            $form = $session->get('formData');
            $model->load($form);
            $step = (int)$form['step'];
        }
    

        if ($model->load(Yii::$app->request->post()) && $model->save(false) ) {

            $payment = $this->payFee($this->apiUrl(), $model->id, $model->account_owner, $model->iban);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if($payment->getStatusCode() == 200)
            {
                $session->remove('formData');
                $data = json_decode($payment->getBody()->getContents());
                $model->payment_data_id = $data->paymentDataId;
                $model->update();
                return [
                    'status' => 'success',
                    'data' => $data->paymentDataId,
                ];
            }
            else{
                return [
                    'status' => 'failure',
                    'data' => $model,
                ];
            }

        }

        $session->close();

        return $this->render('create', [
            'model' => $model,
            'step'=>$step
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Store form data to session
     * 
     */
    public function actionStoreInSession()
    {
        $get = Yii::$app->request->get();
        $get['step']++;
        $session = Yii::$app->session;
        $session->set('formData', $get);
        $session->close();
        return true;

    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

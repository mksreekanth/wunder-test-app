<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\Models\User */

?>
<div class="user-create">

	<h1 class="main-head">Register Now</h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?php 
$this->registerJs('var step = '. $step.';rootUrl ="' .Url::to('@web/', true). '";',  \yii\web\View::POS_HEAD); 
$this->registerJsFile(
    '@web/js/custom.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerCssFile('@web/css/smart_wizard_theme_dots.css', [
    'depends' => [\yii\web\JqueryAsset::className()]
]);

?>


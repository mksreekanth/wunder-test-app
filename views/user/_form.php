<?php

use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin([
        'id' => 'regForm',
        'enableClientValidation' => false,
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{input}\n{hint}\n{error}",
            'horizontalCssClasses' => [
                'error' => ' with-errors',
                'hint' => 'with-errors',
                'label' => ''
            ],
        ],
        // 'errorCssClass' => 'error'
    ]); ?>

    <!-- SmartWizard html -->
    <div id="smartwizard" class="sw-main sw-theme-dots">
        <ul class="nav nav-tabs step-anchor">
            <li class="nav-item active"><a href="#step-1" class="nav-link">Step 1<br><small>Personal Information</small></a></li>
            <li class="nav-item"><a href="#step-2" class="nav-link">Step 2<br><small>Address Information</small></a></li>
            <li class="nav-item"><a href="#step-3" class="nav-link">Step 3<br><small>Payment Information</small></a></li>
            
        </ul>

        <div class="sw-container tab-content" style="min-height: 152px;">
            <div id="step-1" class="tab-pane step-content" style="display: block;">
                
                <div id="form-step-0" role="form" data-toggle="validator">

                    <div class="form-group">
                        <?= $form->field($model, 'first_name', ['options' => ['tag' => 'div']])->textInput(['maxlength' => true, "required" => ""]) ?>
                    </div>

                    <div class="form-group">
                        <?= $form->field($model, 'last_name', ['options' => ['tag' => 'div']])->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="form-group">
                        <?= $form->field($model, 'telephone', ['options' => ['tag' => 'div']])->textInput(['maxlength' => true, "required" => ""]) ?>
                    </div>


                </div>

            </div>
            <div id="step-2" class="tab-pane step-content">
                <div id="form-step-1" role="form" data-toggle="validator">
                    <div class="form-group">
                        <?= $form->field($model, 'street', ['options' => ['tag' => 'div']])->textInput(['maxlength' => true, "required" => ""]) ?>
                    </div>
                    <div class="form-group">
                        <?= $form->field($model, 'house_no', ['options' => ['tag' => 'div']])->textInput(['maxlength' => true, "required" => ""]) ?>
                    </div>
                    <div class="form-group">
                        <?= $form->field($model, 'zip', ['options' => ['tag' => 'div']])->textInput(['maxlength' => true, "required" => ""]) ?>
                    </div>
                    <div class="form-group">
                        <?= $form->field($model, 'city', ['options' => ['tag' => 'div']])->textInput(['maxlength' => true, "required" => ""]) ?>
                    </div>
                </div>
            </div>
            <div id="step-3" class="tab-pane step-content">
                <div id="form-step-2" role="form" data-toggle="validator">
                    <div class="form-group">
                        <?= $form->field($model, 'account_owner', ['options' => ['tag' => 'div']])->textInput(['maxlength' => true, "required" => ""]) ?>
                    </div>
                    <div class="form-group">
                        <?= $form->field($model, 'iban', ['options' => ['tag' => 'div']])->textInput(['maxlength' => true, "required" => ""]) ?>
                    </div>
                    
                </div>
            </div>
        </div>    
    </div>
        
    <?php ActiveForm::end(); ?>
</div>
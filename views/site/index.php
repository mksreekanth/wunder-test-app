<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Wunder Test Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1 class="lead">Welcome to <?= $this-> title ?></h1>


        <p>
            <?= Html::a('Register Now', ['/user/create'], ['class'=>'btn btn-lg btn-success']) ?>
        </p>
    </div>

</div>

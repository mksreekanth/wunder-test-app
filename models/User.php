<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $telephone
 * @property string $street
 * @property string $house_no
 * @property string $zip
 * @property string $city
 * @property string $account_owner
 * @property string $iban
 * @property string $payment_data_id
 * @property string $created_at
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'telephone', 'street', 'house_no', 'zip', 'city', 'account_owner', 'iban'], 'required'],
            [['payment_data_id'], 'string'],
            [['created_at'], 'safe'],
            [['first_name', 'last_name', 'telephone', 'street', 'house_no', 'zip', 'city', 'account_owner', 'iban'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'telephone' => 'Telephone',
            'street' => 'Street',
            'house_no' => 'House No',
            'zip' => 'Zip',
            'city' => 'City',
            'account_owner' => 'Account Owner',
            'iban' => 'IBAN',
            'payment_data_id' => 'Payment Data ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * remove ModelName[] from form inputs name
     */
    public function formName()
    {
        return '';
    }
}

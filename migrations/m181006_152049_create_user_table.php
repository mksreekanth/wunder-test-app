<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m181006_152049_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string(),
            'telephone' => $this->string()->notNull(),
            'street' => $this->string()->notNull(),
            'house_no' => $this->string()->notNull(),
            'zip' => $this->string()->notNull(),
            'city' => $this->string()->notNull(),
            'account_owner' => $this->string()->notNull(),
            'iban' => $this->string()->notNull(),
            'payment_data_id' => $this->text(),
            'created_at' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}

This app is created using Yii2 framework.
Created as a part of a test for Wunder Mobility.

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.


INSTALLATION
------------

Clone the app using the command below or download as zip.

~~~
git clone https://mksreekanth@bitbucket.org/mksreekanth/wunder-test-app.git
~~~

Open terminal in the project directory.
Now install the 3rd party packages by running

~~~
composer update
~~~

Create a database named wunder_test and run

~~~
php yii migrate
~~~

This will create the required tables. You can import the wunder_test.sql file manually too.
(Change the database name or credential if you need in config/db.php)


Start server

~~~
php yii serve
~~~

Now you can open the app at the url http://localhost:8080

OR 

You can directly access via http://localhost/wunder-test-app/web


DESCRIPTION
-----------

This app lets the user register in 3 steps
Step 1: Personal information
- Firstname, lastname, telephone
Step 2: Address information
- Address including street, house number, zip code, city
Step 3: Payment information
- Account owner
- IBAN 

When clicking the �next� button, the inserted data is saved in the database(mysql) and the payment data is displayed.


The user is always redirected to the last opened step when he�s joining the process. The enterd form data is stored in HTML5 
local storage if it is supported. Otherwise the data is stored in session.


**NOTES**
Most of the modern browsers supports local storage. Hence I have implemented local storage as the primary choice for storing form data.
This enhances performance also.


FOR PERFORMANCE OPTIMIZATION
----------------------------

The external assets(js and css fles) can be minified.


THINGS THAT COULD HAVE DONE BETTER
----------------------------------

The UI could be done better.